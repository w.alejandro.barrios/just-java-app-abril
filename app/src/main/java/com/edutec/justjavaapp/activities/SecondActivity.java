package com.edutec.justjavaapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.edutec.justjavaapp.ApplicationJustJavaApp;
import com.edutec.justjavaapp.R;

public class SecondActivity extends AppCompatActivity {

    TextView txtSegundo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        /* String request[] = getIntent().getExtras().getStringArray("titulo"); */
        String request[] = ApplicationJustJavaApp.misStrings; // USO DEL APPLICATION
        txtSegundo = findViewById(R.id.txtSegundo);
        for (int contador = 0; contador < request.length; contador++){
            System.out.println(request[contador] + " Posicion " + contador);
        }
        txtSegundo.setText(request[0]);

        String[][] arrayBidimensional = new String[5][2];
        arrayBidimensional[0][0] = "dato 0,0";
        arrayBidimensional[0][1] = "dato 0,1";
        arrayBidimensional[1][0] = "dato 1,0";
        arrayBidimensional[1][1] = "dato 1,1";

        for (int contadorArray = 0; contadorArray < arrayBidimensional.length; contadorArray++){
            if (arrayBidimensional[contadorArray][0] != null)
                System.out.println(arrayBidimensional[contadorArray][0]);
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Toast.makeText(this, "VOLVER", Toast.LENGTH_SHORT).show();

    }
}
