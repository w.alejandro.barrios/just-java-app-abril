package com.edutec.justjavaapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.edutec.justjavaapp.R;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {

    private static final long SCREEN_DELAY = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getSupportActionBar().hide();

        /* Timer timer = new Timer();

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Intent cambiarPantalla = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(cambiarPantalla);
                finish();
            }
        };

        timer.schedule(timerTask, SCREEN_DELAY); */

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Intent cambiarPantalla = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(cambiarPantalla);
                finish();
            }
        }, SCREEN_DELAY);

    }
}
