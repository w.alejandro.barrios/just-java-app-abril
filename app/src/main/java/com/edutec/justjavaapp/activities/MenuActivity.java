package com.edutec.justjavaapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.edutec.justjavaapp.ApplicationJustJavaApp;
import com.edutec.justjavaapp.R;
import com.edutec.justjavaapp.adapters.HamburguesaAdapter;
import com.edutec.justjavaapp.beans.Hamburguesa;

import java.util.ArrayList;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        ApplicationJustJavaApp.hamburguesas = new ArrayList<Hamburguesa>();

        /*String text = "asd-fgh-jkl";
        String[] textoSeparado = text.split(",");
        String nuevo = textoSeparado[2]; */

        final ArrayList<Hamburguesa> hamburguesaArrayList = new ArrayList<Hamburguesa>();
        Hamburguesa quesoburguesa = new Hamburguesa();
        quesoburguesa.setNombre("Quesoburguesa");
        quesoburguesa.setPrecio(15.50);
        quesoburguesa.setImg(R.drawable.hamburger);

        Hamburguesa doble = new Hamburguesa("Hamburguesa doble", 20.50, R.drawable.hamburger);

        hamburguesaArrayList.add(quesoburguesa);
        hamburguesaArrayList.add(doble);
        hamburguesaArrayList.add(new Hamburguesa("BigMac", 30.5, R.drawable.hamburger));
        hamburguesaArrayList.add(new Hamburguesa("Mac pollo", 30.5, R.drawable.hamburger));

        HamburguesaAdapter hamburguesaAdapter = new HamburguesaAdapter(this, hamburguesaArrayList);

        ListView listView = findViewById(R.id.listView);
        listView.setAdapter(hamburguesaAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                switch (position){
                    case 0 :
                        ApplicationJustJavaApp.hamburguesas.add(hamburguesaArrayList.get(position));
                        break;

                    case 1:
                        Intent intent2 = new Intent(getApplicationContext(), SecondActivity.class);
                        startActivity(intent2);
                        break;

                    case 2:

                        break;

                    case 3:

                        break;
                }
            }
        });

    }
}
