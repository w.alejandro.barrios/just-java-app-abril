package com.edutec.justjavaapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.edutec.justjavaapp.ApplicationJustJavaApp;
import com.edutec.justjavaapp.R;

public class MainActivity extends AppCompatActivity {

    int cantidad;
    TextView textCantidad;
    TextView txtPrecio;
    EditText edtReceptor;
    Button btnFinalizar;
    final static int PRECIO = 5; // CONSTANTE DE PRECIO

    public void mostrar(){
        Toast.makeText(this, "HOLA", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(this, "CREADA", Toast.LENGTH_SHORT).show();

        cantidad = 1;
        textCantidad = findViewById(R.id.txtCantidad);
        txtPrecio = findViewById(R.id.txtPrecio);
        edtReceptor = findViewById(R.id.edtReceptor);
        btnFinalizar = findViewById(R.id.btnFinalizar);
        textCantidad.setText("" + cantidad);
        txtPrecio.setText("" + subtotal());

        btnFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finalizar();

            }
        });

        btnFinalizar.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(getApplicationContext(), "LONG CLICK", Toast.LENGTH_LONG).show();
                return true;
            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "ESTOY DESTRUIDA", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //Toast.makeText(this, "REINICIADA", Toast.LENGTH_SHORT).show();
    }

    public void restar(View view) {
        mostrar();
        switch (cantidad) {
            case 10:
            case 9:
            case 8:
            case 7:
            case 6:
                cantidad--;
                break;
            case 5:
                cantidad = cantidad - 2;
                break;
        }
        textCantidad.setText("" + cantidad);
        txtPrecio.setText(Integer.toString(subtotal()));
    }

    public void sumar(View view) {
        if (cantidad <= 9) {
            cantidad++; // cantidad = cantidad + 1; TRUE
            if (cantidad == 8)
                Toast.makeText(this, "SOY 8", Toast.LENGTH_LONG).show();

        } else {
            // FALSE
            Toast.makeText(this, "Ya no puedes ordenar mas", Toast.LENGTH_LONG).show();
        }
        textCantidad.setText(Integer.toString(cantidad));
        int result = subtotal();
        txtPrecio.setText("" + result);
    }

    public int subtotal() {
        int resultado = 0;
        //if (cantidad > 0 && cantidad < 7)
        for (int contador = 0; contador < cantidad; contador++) {
            resultado = resultado + PRECIO;
        }

        return resultado;
    }

    public void finalizar() {

        String dato = edtReceptor.getText().toString();
        /*String[] arrayStrings = new String[5];
        arrayStrings[0] = dato;
        arrayStrings[1] = "Dato 2";
        arrayStrings[2] = "Dato 3";
        arrayStrings[3] = "Dato 4";
        arrayStrings[4] = "Dato 5"; */

        // USO DEL APPLICATION
        ApplicationJustJavaApp.misStrings = new String[5];
        ApplicationJustJavaApp.misStrings[0] = dato;
        ApplicationJustJavaApp.misStrings[1] = "Dato 2";
        ApplicationJustJavaApp.misStrings[2] = "Dato 3";
        ApplicationJustJavaApp.misStrings[3] = "Dato 4";
        ApplicationJustJavaApp.misStrings[4] = "Dato 5";

        // Intent explícito

        Intent cambiarActividad = new Intent(this, SecondActivity.class);
        /* cambiarActividad.putExtra("titulo", arrayStrings); */
        startActivity(cambiarActividad);
        //finish();

        // Intent implícito
        /* String url = "https://www.facebook.com";
        Intent visitarWeb = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.parse(url);
        visitarWeb.setData(uri);
        startActivity(visitarWeb); */
    }
}
