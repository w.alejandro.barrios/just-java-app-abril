package com.edutec.justjavaapp.beans;

public class Hamburguesa {

    private String nombre;
    private double precio;
    private int img;

    public Hamburguesa(){

    }

    public  Hamburguesa(String nombre, double precio, int img){
        this.nombre = nombre;
        this.precio = precio;
        this.img = img;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getNombre(){
        return nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
