package com.edutec.justjavaapp.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.edutec.justjavaapp.R;
import com.edutec.justjavaapp.beans.Hamburguesa;

import java.util.ArrayList;

public class HamburguesaAdapter extends ArrayAdapter<Hamburguesa> {

    public HamburguesaAdapter(Activity context, ArrayList<Hamburguesa> hamburguesasList){
        super(context, 0, hamburguesasList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.hamburguesa_item, parent, false);
        }

        // Hamburguesa actual
        Hamburguesa currentHamburguesa = getItem(position);

        // BUSCAR ELEMENTOS DEL XML
        TextView txtNombre = listItemView.findViewById(R.id.txtNombre);
        TextView txtPrecio = listItemView.findViewById(R.id.txtPrecio);
        ImageView imgMuestra = listItemView.findViewById(R.id.imgHamburguesa);

        // CARGARLE ELEMENTOS
        txtNombre.setText(currentHamburguesa.getNombre());
        txtPrecio.setText("" + currentHamburguesa.getPrecio());
        imgMuestra.setImageResource(currentHamburguesa.getImg());

        return listItemView;
    }
}
